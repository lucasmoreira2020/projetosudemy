package entities;

public class Conta {

    private String nome;
    private Integer agencia;
    private Integer conta;

    public Conta(String nome, int agencia, int conta) {
        super();
        this.nome = nome;
        this.agencia = agencia;
        this.conta = conta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

    public Integer getConta() {
        return conta;
    }

    public void setConta(Integer conta) {
        this.conta = conta;
    }

    public String toString(){
        return "Nome: " + getNome() + ", Agencia: " + getAgencia() + ", Conta: " + getConta();
    }

}
