package application;

import entities.Conta;

import java.util.Locale;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.print("Informeu seu nome: ");
        String nome = sc.nextLine();

        System.out.print("Informe o número da agencia:");
        int agencia = sc.nextInt();

        System.out.print("Informe o número da conta: ");
        int conta = sc.nextInt();

        Conta co = new Conta(nome,agencia,conta);
        co.setNome(nome);
        co.setAgencia(agencia);
        co.setConta(conta);
        System.out.println(co.toString());



        System.out.println();



        sc.close();
    }
}
